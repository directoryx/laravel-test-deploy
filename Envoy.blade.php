@servers(['web' => 'web@173.82.240.11'])

{{-- Test Envoy --}}
{{-- @task('list', [on => 'web'])
    ls -l
@endtask --}}

@setup
    $repository = 'git@gitlab.com:directoryx/laravel-test-deploy.git';
    $releases_dir = '/var/www/releases';
    $app_dir = '/var/www/';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    chmod_775
    move_env
    update_symlinks    
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    echo 'Done'
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}
    composer install 
    echo 'Done'
@endtask

@task('update_symlinks')
    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current
    echo 'Done'
@endtask


@task('chmod_775')
    echo "Changing Access to 775"
    cd {{ $new_release_dir }}
    chmod 775 -R *
    echo "Done"
@endtask

@task('move_env')
    echo "Move to directory project"
    cd {{ $new_release_dir }}
    echo "copy .env.example"
    cp .env.example .env
    echo "generate key"
    php artisan key:generate
    echo "Done"
@endtask